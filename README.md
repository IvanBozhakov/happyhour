System use IoC laravel module and Dependency Injection package. For storing in Memcache include (you are have to install memcached):

Requirements
1.Install composer.json in app/ folder
2.The example can be tested with memcache or filesystem
Inserting configuration file and autoload


```
#!php

require_once('start.php');

require_once('app/vendor/autoload.php');
require 'CacheHappy.php';

```

If use file system :


```
#!php

use Illuminate\Cache\FileStore;
use Illuminate\Filesystem\Filesystem;
```


Create new container and set in memcache


```
#!php

$container = new Container();

/**
 * Create CacheHappy Instace
 */

$cache = $container->make('CacheHappy');

$container->bind('happy', function() use ($cache) {
    return new HappyHour($cache);
});

$happyCache = $container->make('happy');
```

Set in File system


```
#!php

/**
 * 
 * Create new container who store in file system
 * Create FileStore instance and set store folder
 */
$fileSystem = new Filesystem;
$fileCache = new FileStore($fileSystem, 'store/cache');

$container->bind('happyFile', function() use ($fileCache) {
    return new HappyHour($fileCache);
});

$happyFile = $container->make('happyFile');
```