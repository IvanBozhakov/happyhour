<?php

/**
 * Include configuration file
 */
require_once('start.php');
/**
 * Include outside modules
 */
require 'app/vendor/autoload.php';
/**
 * Include Memcache module
 */
include 'CacheHappy.php';

/**
 * Declare using namespace
 */
use Illuminate\Container\Container;
/**
 * Users can choose filestore
 */
use Illuminate\Cache\FileStore;
use Illuminate\Filesystem\Filesystem;

/**
 * First create new  Container
 */
$container = new Container();

/**
 * Create CacheHappy Instace
 */
$cache = $container->make('CacheHappy');
echo "Saving in Memcache <br/>";
/**
 * Create Happy Hour container who store in Memcache
 * @param StoreInterface $cache
 */
$container->bind('happy', function() use ($cache) {
    return new HappyHour($cache);
});
$happyCache = $container->make('happy');
try {
    echo '<pre>' . print_r($happyCache->getHappyHour(), true) . '</pre>';
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}


if ($happyCache->isHappyHour()){
    echo "HappyHour<br/>";
}
echo "Saving in File System <br/>";
/**
 * 
 * Create new container who store in file system
 * Create FileStore instace and set store folder
 */
$fileSystem = new Filesystem;
$fileCache = new FileStore($fileSystem, 'store/cache');
/**
 * Create Happy Hour container who store in filesystem
 *  @param StoreInterface $cache
 */
$container->bind('happyFile', function() use ($fileCache) {
    return new HappyHour($fileCache);
});
$happyFile = $container->make('happyFile');

echo '<pre>' . print_r($happyFile->getHappyHour(), true) . '</pre>';
if ($happyFile->isHappyHour()){
    echo "HappyHour";
}

