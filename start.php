<?php
require_once('HappyHourInterface.php');
require_once('HappyHour.php');

$config = array(
    /**
     * Global settin for turn system
     */
    'turn' => 'on',
    /*
     * Happy hour bonus in percent 
     */
    'bonus' => 20,
    /**
     * Start date for HH in date time format 'Y-m-d H:i:s'
     */
    'dateStart' =>'2014-12-10 12:00:00',
    /*
     * Stop date for HH in date time format 'Y-m-d H:i:s'
     */
    'dateEnd' => '2015-02-03 12:00:00',
    /**
     * List of times in which there might be happyhour
     * In DateTime format 'H:i:s'
     */
    'list' => array(
        '07:00:00',
        '08:00:00',
        '10:00:00',
        '13:00:00',
        '14:00:00',
        '15:00:00',
        '16:00:00',
        '17:00:00',
        '18:00:00',
        '19:00:00',
        '22:00:00'),
    /**
     * Set Happy Hour duration in minutes
     */
    'duration' => 60,
    /**
     * Set current time zone
     */
    'timezone' => date_default_timezone_set('Europe/Sofia')
);

/**
 * @param array config set global configuration for system
 * @return void 
 */
   HappyHour::runSystem($config); 


