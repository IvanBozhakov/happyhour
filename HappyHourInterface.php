<?php
interface HappyHourInterface 
{

	/**
	 * Checks to see if there is happy hour
	 * going on at the moment.
	 * 
	 * @return bool
	 */
	public function isHappyHour();

	/**
	 * Retrieves the bonus value of the happy hour
	 * if the Happy Hour is running
	 * 
	 * EXAMPLE HH ON: getBonusCredits(100) = 120
	 * EXAMPLE HH OFF: getBonusCredits(100) = 100
	 * 
	 * @param int $credits
	 * @return int
	 */
	public function getBonusCredits($credits = 0);
}
