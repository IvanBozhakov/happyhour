<?php

use Illuminate\Cache\StoreInterface;

class CacheHappy implements StoreInterface
{

    /**
     * Memcache instace
     * @var Memcache $cache
     */
    private $cache = null;

    /**
     * Create Memcache object
     * @param string $host
     * @param int $port
     * @return void
     */
    public function __construct($host = "localhost", $port = 11211)
    {
        if ($this->cache == null) {
            $this->cache = new Memcache;
            $this->cache->connect($host, $port);
        }
    }

    /**
     * Retrieve an item from the cache by key.
     *
     * @param  string  $key
     * @return mixed
     */
    public function get($key)
    {
        $key = trim($key);
        if (strtotime($key) && !empty($key)) {
            return $this->cache->get($key);
        } else {
            throw new InvalidArgumentException("Set date is not valid");
        }
    }

    /**
     * Store an item in the cache for a given number of minutes.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @param  int     $minutes
     * @return void
     */
    public function put($key, $value, $time)
    {
        $time = ceil($time * 60);
        if (!empty($key) && !empty($value)) {
            $this->cache->set($key, $value, MEMCACHE_COMPRESSED, $time);
        }
    }

    public function increment($key, $value = 1)
    {
        
    }

    /**
     * Decrement the value of an item in the cache.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return int|bool
     */
    public function decrement($key, $value = 1)
    {
        
    }

    /**
     * Store an item in the cache indefinitely.
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return void
     */
    public function forever($key, $value)
    {
        
    }

    /**
     * Remove an item from the cache.
     *
     * @param  string  $key
     * @return bool
     */
    public function forget($key)
    {
        
    }

    /**
     * Remove all items from the cache.
     *
     * @return void
     */
    public function flush()
    {
        
    }

    /**
     * Get the cache key prefix.
     *
     * @return string
     */
    public function getPrefix()
    {
        
    }

}
